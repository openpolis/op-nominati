import logging

import agate
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from nominati.models import Regione, Cpt_Categoria, Cpt_Sottocategoria, \
    Cpt_Sottotipo, Cpt_Settore_Partecipata, Partecipata, Cpt_Settore

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Command(BaseCommand):
    """
       Task per importare la lista di partecipazioni dal CSV del Ministero
       Pubblica Amministrazione e dati dal CPT
    """
    help = 'Import comma-separated values from file'
    requires_migrations_checks = True

    csv_file = ''
    errors_list = []

    def add_arguments(self, parser):
        parser.add_argument('--csv-file',
                            dest='csvfile',
                            default=None,
                            help='Select csv file'),
        parser.add_argument('--type',
                            dest='type',
                            default=None,
                            help='Type of import: part|cpt_part'),
        parser.add_argument('--year',
                            dest='year',
                            help='Year of import: eg.2012'),
        parser.add_argument('--update',
                            dest='update',
                            action='store_true',
                            default=False,
                            help='Update Existing Records: True|False'),
        parser.add_argument('--delete',
                            dest='delete',
                            action='store_true',
                            default=False,
                            help='Delete Existing Records: True|False'),

    csv_file = ''
    unicode_reader = None
    unicode_writer = None
    encoding = None

    def handle(self, *args, **options):
        if options['csvfile'] is None:
            logger.error("CSV file is needed\n")
            exit(1)
        self.csv_file = options['csvfile']
        if options['type'] == 'cpt_part':
            self.handle_cptpart(*args, **options)
        else:
            logger.error(
                "Wrong type %s. Select among part,cpt_part." % options['type'])
            exit(1)

    def handle_cptpart(self, *args, **options):
        logger.info('CSV file "%s"\n' % self.csv_file)
        self.encoding = 'utf-8'
        # read csv file
        try:
            logger.info("Read CSV...")

            type_tester = agate.TypeTester(force={
                'CPT_COD_CATEGORIA': agate.Text(),
                'CPT_COD_SOTTOTIPO': agate.Text()
            })

            table = agate.Table.from_csv(self.csv_file,
                                         column_types=type_tester)

            print(table)
        except IOError:
            logger.error(
                "It was impossible to open file %s\n" % self.csv_file)
            exit(1)
        # except csv.Error, e:
        #   logger.error(
        #        "CSV error while reading %s: %s\n" % (self.csv_file, e.message))
        c = 0
        logger.info("Inizio import da %s" % self.csv_file)

        # Totale
        # CPT_Anno_Produzione_BancaDati
        # DESCRIZIONE_ENTE
        # IDFISC_ENTE
        # INDIRIZZO_ENTE
        # CAP
        # COMUNE
        # PROVINCIA
        # REGIONE
        # CPT_Universo_riferimento
        # CPT_COD_CATEGORIA
        # CPT_DESCR_CATEGORIA
        # CPT_COD_SOTTOTIPO
        # CPT_DESCR_SOTTOTIPO
        # CPT_Primo_Anno_Rilevazione
        # CPT_Ultimo_Anno_Rilevazione
        # CPT_Settore01 ... CPT_Settore12
        for row in table.rows:
            id_fisc = row['IDFISC_ENTE']
            if id_fisc is not None and len(id_fisc) <= 11:
                # zero padding for codice fiscale
                cf_partecipata = id_fisc.zfill(11)
                #  if partecipata is not in the db, insert new Partecipata
                nome_regione = row['REGIONE'].strip()
                # fixes for CSV errors
                if nome_regione == 'Friuli Venezia Giulia':
                    nome_regione = 'FRIULI-VENEZIA GIULIA'
                if nome_regione == 'Emilia Romagna':
                    nome_regione = 'EMILIA-ROMAGNA'
                if nome_regione == 'Provincia Autonoma di Bolzano' or nome_regione == 'Provincia Autonoma di Trento':
                    nome_regione = 'TRENTINO-ALTO ADIGE'
                regione, regione_created = Regione.objects.get_or_create(
                    denominazione=nome_regione.upper())
                if regione_created:
                    logger.info("Regione non presente, creata regione: %s",
                                regione.denominazione)

                cpt_cod_categoria = row['CPT_COD_CATEGORIA']
                cpt_cod_sottocategoria = row['CPT_COD_SOTTOTIPO'][3]
                cpt_cod_sottotipo = row['CPT_COD_SOTTOTIPO'][4:]
                cpt_categoria, categoria_created = Cpt_Categoria.objects.get_or_create(
                    codice=cpt_cod_categoria,
                    defaults={
                        'denominazione': row['CPT_DESCR_CATEGORIA']
                    }
                )
                if categoria_created:
                    logger.info("%s: CPT Categoria inserita: %s" % (
                        cpt_cod_categoria, row['CPT_DESCR_CATEGORIA']))
                # sottocategoria
                cpt_sottocategoria, sottocategoria_created = Cpt_Sottocategoria.objects.get_or_create(
                    categoria=cpt_categoria,
                    codice=cpt_cod_sottocategoria,
                )
                if sottocategoria_created:
                    logger.info(
                        "%s: CPT sottocategoria inserita: %s" % (
                            cpt_cod_sottocategoria, ''))
                # sottotipo
                cpt_sottotipo, sottotipo_created = Cpt_Sottotipo.objects.get_or_create(
                    sottocategoria=cpt_sottocategoria,
                    codice=cpt_cod_sottotipo,
                    defaults={
                        'denominazione': row['CPT_DESCR_SOTTOTIPO'],
                    }
                )
                if sottotipo_created:
                    logger.info("%s: CPT sottotipo inserito: %s" % (
                        cpt_cod_sottotipo, row['CPT_DESCR_SOTTOTIPO']))

                provincia = row['PROVINCIA']
                if provincia is None or len(provincia) > 2:
                    provincia = ''
                else:
                    provincia = provincia.strip().upper()
                    if provincia == 'Latina':
                        provincia = 'LT'
                logger.info('Provincia: %s', provincia)
                partecipata, partecipata_created = Partecipata.objects.get_or_create(
                    denominazione=row['DESCRIZIONE_ENTE'],
                    codice_fiscale=id_fisc,
                    defaults={
                        'indirizzo': row['INDIRIZZO_ENTE'],
                        'comune': row['COMUNE'],
                        'cap': row['CAP'],
                        'provincia': provincia,
                        'regione': regione,
                        'cpt_universo_riferimento': row[
                            'CPT_Universo_riferimento'],
                        'cpt_primo_anno_rilevazione': row[
                            'CPT_Primo_Anno_Rilevazione'],
                        'cpt_ultimo_anno_rilevazione': row[
                            'CPT_Ultimo_Anno_Rilevazione'],
                        'cpt_categoria': cpt_categoria,
                        'cpt_sottocategoria': cpt_sottocategoria,
                        'cpt_sottotipo': cpt_sottotipo
                    }
                )
                if partecipata_created:
                    logger.info("%s: Partecipata inserita: %s" % (
                        cf_partecipata, row['DESCRIZIONE_ENTE']))
                else:
                    if options['update']:
                        partecipata.denominazione = row['DESCRIZIONE_ENTE']
                        partecipata.indirizzo = row['INDIRIZZO_ENTE']
                        partecipata.comune = row['COMUNE']
                        partecipata.cap = row['CAP']
                        partecipata.provincia = row['PROVINCIA']
                        partecipata.regione = regione
                        partecipata.cpt_universo_riferimento = row[
                            'CPT_Universo_riferimento']
                        partecipata.cpt_primo_anno_rilevazione = row[
                            'CPT_Primo_Anno_Rilevazione']
                        partecipata.cpt_ultimo_anno_rilevazione = row[
                            'CPT_Ultimo_Anno_Rilevazione']
                        partecipata.cpt_categoria = cpt_categoria
                        partecipata.cpt_sottocategoria = cpt_sottocategoria
                        partecipata.cpt_sottotipo = cpt_sottotipo
                        partecipata.save()
                        logger.info(
                            "%s: Partecipata presente aggiornata: %s" % (
                                cf_partecipata, row['DESCRIZIONE_ENTE']))
                    else:
                        logger.info("%s: Partecipata presente: %s" % (
                            cf_partecipata, row['DESCRIZIONE_ENTE']))
                # cpt settore
                if not partecipata_created and options['update']:
                    Cpt_Settore_Partecipata.objects.filter(
                        partecipata=partecipata).delete()
                if partecipata_created or options['update']:
                    for i in range(1, 13):
                        cpt_settore_i = "CPT_Settore" + str(i).zfill(2)
                        if row[cpt_settore_i] is not None and len(
                            row[cpt_settore_i]) > 0:
                            # adds cpt settore
                            cpt_settore_cod = row[cpt_settore_i][0:5]
                            cpt_settore, cpt_settore_created = Cpt_Settore.objects.get_or_create(
                                codice=cpt_settore_cod,
                                defaults={
                                    'denominazione': row[cpt_settore_i][8:]
                                }
                            )
                            if cpt_settore_created:
                                logger.info(
                                    "%s: CPT settore creato: %s" % (
                                        cpt_settore_cod,
                                        row[cpt_settore_i][8:]))
                            cpt_settore_part = Cpt_Settore_Partecipata()
                            cpt_settore_part.partecipata = partecipata
                            cpt_settore_part.settore = cpt_settore
                            cpt_settore_part.save()
                            logger.info(
                                "%s: CPT settore partecipata inserito: %s" % (
                                    partecipata.denominazione,
                                    cpt_settore.codice))
                        else:
                            break
        c = +1
