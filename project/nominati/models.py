from datetime import datetime

from django.db import models
from django.db.models import Q, Sum
from django.db.models.manager import BaseManager

from nominati.managers import TimeFramedQuerySet


class Comparto(models.Model):
    denominazione = models.CharField(max_length=255)
    tipo = models.CharField(max_length=32, blank=True, null=True)

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Comparti'


class Bilancio(models.Model):
    RESOCONTO = (
        (0, 'Pareggio'),
        (1, 'Avanzo'),
        (-1, 'Perdita'),
    )
    anno = models.CharField(max_length=4)
    resoconto = models.IntegerField(verbose_name='Risultato di esercizio',
                                    choices=RESOCONTO)
    dettaglio = models.IntegerField(
        verbose_name='Importo del risultato di esercizio', blank=True,
        null=True)
    patrimonio_netto = models.IntegerField(verbose_name='Patrimonio netto',
                                           blank=True, null=True)
    partecipata = models.ForeignKey('Partecipata')

    class Meta:
        verbose_name_plural = 'Bilanci'


class Partecipata(models.Model):
    denominazione = models.CharField(max_length=255)
    codice_fiscale = models.CharField(max_length=11)
    MACRO_TIPOLOGIA = (
        ('SOCIETA\'', 'Società'),
        ('CONSORZIO', 'Consorzio'),
        ('FONDAZIONE', 'Fondazione')
    )
    UNIVERSO_RIFERIMENTO = (
        ('PA', 'PA'),
        ('ExtraPA', 'ExtraPA')
    )
    macro_tipologia = models.CharField(max_length=32, choices=MACRO_TIPOLOGIA)
    tipologia_partecipata = models.ForeignKey('TipologiaPartecipata', null=True,
                                              on_delete=models.PROTECT)
    competenza_partecipata = models.ForeignKey('CompetenzaPartecipata',
                                               null=True,
                                               on_delete=models.SET_NULL)
    finalita_partecipata = models.ForeignKey('FinalitaPartecipata', null=True,
                                             on_delete=models.SET_NULL)
    url = models.URLField(blank=True, null=True)
    indirizzo = models.CharField(max_length=100, null=True)
    cap = models.CharField(max_length=5, null=True)
    comune = models.CharField(max_length=50, null=True)
    provincia = models.CharField(max_length=3, null=True)
    regione = models.ForeignKey('Regione', on_delete=models.SET_NULL, null=True)
    cpt_categoria = models.ForeignKey('Cpt_Categoria', null=True,
                                      on_delete=models.SET_NULL)
    cpt_sottocategoria = models.ForeignKey('Cpt_Sottocategoria', null=True,
                                           on_delete=models.SET_NULL)
    cpt_sottotipo = models.ForeignKey('Cpt_Sottotipo', null=True,
                                      on_delete=models.SET_NULL)
    cpt_universo_riferimento = models.CharField(max_length=8,
                                                choices=UNIVERSO_RIFERIMENTO,
                                                null=True)
    cpt_primo_anno_rilevazione = models.CharField(max_length=5, null=True,
                                                  blank=True, default=None)
    cpt_ultimo_anno_rilevazione = models.CharField(max_length=5, null=True,
                                                   blank=True, default=None)

    def ultimo_bilancio(self):
        bilanci = self.bilancio_set.all().order_by('-anno')[:1]
        return bilanci[0] if len(bilanci) else None

    @property
    def numero_incarichi_attivi(self):
        now = datetime.now()
        return self.incarico_set.filter(
            Q(data_inizio__lte=now) &
            (Q(data_fine__gte=now) | Q(data_fine__isnull=True))).count()

    @property
    def totale_compensi_incarichi_attivi(self):
        now = datetime.now()
        return self.incarico_set.filter(
            Q(data_inizio__lte=now) &
            (Q(data_fine__gte=now) | Q(data_fine__isnull=True))).aggregate(
            s=Sum('compenso_totale'))['s']

    def partecipata_da(self, anno):
        return self.partecipazione_set.filter(anno=anno).order_by(
            'partecipata__denominazione')

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Partecipate'


class Incarico(models.Model):
    persona = models.ForeignKey('Persona')
    ente_nominante_cf = models.ForeignKey('Ente', verbose_name='Ente',
                                          db_column='ente_cf', blank=True,
                                          null=True)
    tipo_carica = models.ForeignKey('TipoCarica')
    partecipata = models.ForeignKey('Partecipata',
                                       verbose_name='Partecipata', blank=True,
                                       null=True)
    compenso_anno = models.IntegerField(verbose_name='Comp. anno', null=True,
                                        blank=True)
    compenso_carica = models.IntegerField(verbose_name='Comp. carica',
                                          null=True, blank=True)
    altri_compensi = models.IntegerField(verbose_name='Altri comp.', null=True,
                                         blank=True)
    compenso_totale = models.IntegerField(verbose_name='Comp. TOT', null=True,
                                          blank=True)
    indennita_risultato = models.IntegerField(verbose_name='Ind. risultato',
                                              null=True, blank=True)
    data_inizio = models.DateField(null=True, blank=True)
    data_fine = models.DateField(null=True, blank=True)
    objects = BaseManager.from_queryset(TimeFramedQuerySet)

    def save(self, *args, **kwargs):
        self.compenso_totale = 0
        if self.compenso_anno:
            self.compenso_totale += self.compenso_anno
        if self.compenso_carica:
            self.compenso_totale += self.compenso_carica
        if self.indennita_risultato:
            self.compenso_totale += self.indennita_risultato
        if self.altri_compensi:
            self.compenso_totale += self.altri_compensi
        if self.indennita_risultato is None \
            and self.compenso_anno is None \
            and self.compenso_carica is None \
            and self.compenso_carica is None \
            and self.altri_compensi is None:
            self.compenso_totale = None
        # Call parent's ``save`` function
        super(Incarico, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Incarichi'


class Cpt_Settore(models.Model):
    denominazione = models.CharField(max_length=255)
    codice = models.CharField(max_length=10)

    def __str__(self):
        return self.codice + ' - ' + self.denominazione

    class Meta:
        verbose_name_plural = 'CPT Settori'
        verbose_name = 'CPT Settore'


class Cpt_Settore_Partecipata(models.Model):
    def __str__(self):
        return self.partecipata.denominazione + ' - ' + self.settore.denominazione

    partecipata = models.ForeignKey('Partecipata', on_delete=models.CASCADE,
                                    null=False, default='')
    settore = models.ForeignKey('Cpt_Settore', on_delete=models.CASCADE,
                                null=False, default='')


class Cpt_Categoria(models.Model):
    denominazione = models.CharField(max_length=255)
    codice = models.CharField(max_length=3)

    def __str__(self):
        return self.codice + ' - ' + self.denominazione

    class Meta:
        verbose_name_plural = 'CPT Categorie'
        verbose_name = 'CPT Categoria'


class Cpt_Sottocategoria(models.Model):
    categoria = models.ForeignKey('Cpt_Categoria', on_delete=models.CASCADE,
                                  null=False)
    codice = models.CharField(max_length=1)
    denominazione = models.CharField(max_length=255)

    def __str__(self):
        return self.categoria.__str__() + ' - ' + self.codice + ' - ' + self.denominazione

    class Meta:
        verbose_name_plural = 'CPT Sottocategorie'
        verbose_name = 'CPT Sottocategoria'


class Cpt_Sottotipo(models.Model):
    denominazione = models.CharField(max_length=255)
    codice = models.CharField(max_length=2)
    sottocategoria = models.ForeignKey('Cpt_Sottocategoria',
                                       on_delete=models.CASCADE, null=False)

    def __str__(self):
        denominazione = self.denominazione
        if len(denominazione) > 90:
            denominazione = denominazione[:90] + '...'
        return 'C:' + self.sottocategoria.categoria.codice + ' - ' + \
               'SC:' + self.sottocategoria.codice + ' - ' + \
               self.codice + ' - ' + denominazione

    class Meta:
        verbose_name_plural = 'CPT Sottotipo'
        verbose_name = 'CPT Sottotipo'


class TipologiaPartecipata(models.Model):
    denominazione = models.CharField(max_length=255)

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Tipologie partecipate'


class CompetenzaPartecipata(models.Model):
    denominazione = models.CharField(max_length=255)

    @property
    def finalita(self):
        return self.finalitapartecipata_set.all()

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Competenze partecipate'


class FinalitaPartecipata(models.Model):
    denominazione = models.CharField(max_length=255)
    competenza_partecipata = models.ManyToManyField(CompetenzaPartecipata,
                                                    db_table='nominati_competenza_finalita')

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Finalità partecipate'


class Ente(models.Model):
    codice_fiscale = models.CharField(max_length=11, primary_key=True)
    denominazione = models.CharField(max_length=255)
    regione = models.ForeignKey('Regione')
    comparto = models.ForeignKey('Comparto')
    partecipata_set = models.ManyToManyField('Partecipata',
                                             through='Partecipazione')
    codice_istat = models.CharField(max_length=6, null=True, blank=True)

    @property
    def partecipate(self):
        return self.partecipata_set.all()

    @property
    def n_partecipate(self):
        return self.partecipata_set.count()

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'Enti'


class Partecipazione(models.Model):
    SENT = (
        ('SI', 'Sì'),
        ('NO', 'No'),
    )
    ente_cf = models.ForeignKey('Ente', verbose_name='Ente',
                                db_column='ente_cf')
    partecipata = models.ForeignKey('Partecipata',
                                       verbose_name='Partecipata')
    anno = models.CharField(max_length=4)
    onere_complessivo = models.CharField(max_length=255, blank=True)
    percentuale_partecipazione = models.FloatField(null=True, blank=True)
    dichiarazione_inviata = models.CharField(max_length=2, choices=SENT,
                                             blank=True)

    class Meta:
        verbose_name = 'Partecipazione'
        verbose_name_plural = 'Partecipazioni'

    def __str__(self):
        return self.ente_cf.__str__() + ' - ' + \
               self.partecipata.__str__() + ' - ' + self.anno


class Persona(models.Model):
    FEMALE_SEX = 0
    MALE_SEX = 1
    SEX = (
        (MALE_SEX, 'Uomo'),
        (FEMALE_SEX, 'Donna'),
    )
    nome = models.CharField(max_length=64)
    cognome = models.CharField(max_length=64)
    data_nascita = models.DateField(null=True, blank=True,
                                    verbose_name='Data di nascita')
    luogo_nascita = models.CharField(max_length=64, null=True, blank=True,
                                     verbose_name='Luogo nasc')
    sesso = models.IntegerField(choices=SEX, blank=True)
    openpolis_id = models.CharField(max_length=10, null=True, blank=True,
                                    verbose_name='op_id')
    openpolis_n_similars = models.IntegerField(default=0,
                                               verbose_name='n_similars')

    @property
    def incarichi(self):
        return self.incarico_set.all()

    @property
    def n_incarichi(self):
        return self.incarico_set.all().count()

    @property
    def op_id(self):
        if self.openpolis_id != '' and self.openpolis_id is not None:
            return int(self.openpolis_id)
        else:
            return None

    def has_op_id(self):
        return True if self.openpolis_id else False

    def __str__(self):
        return self.nome + ' ' + self.cognome

    class Meta:
        verbose_name_plural = 'Persone'
        unique_together = ('nome', 'cognome', 'data_nascita', 'luogo_nascita')


class Regione(models.Model):
    denominazione = models.CharField(max_length=32)

    def __str__(self):
        return self.denominazione

    @property
    def n_partecipate(self):
        return Partecipata.objects.filter(
            ente__regione=self._get_pk_val()).distinct().count()

    class Meta:
        verbose_name_plural = 'Regioni'


class TipoCarica(models.Model):
    denominazione = models.CharField(max_length=64)
    descrizione = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name = 'Tipo incarico'
        verbose_name_plural = 'Tipi incarico'
