from django.shortcuts import render
from django.views import generic

from nominati import models


class Partecipata:
    class PartecipataView(generic.DetailView):
        model = models.Partecipata

    def list(request):
        partecipate = models.Partecipata.objects \
            .values('denominazione', 'regione',
                    'provincia', 'comune', 'id') \
            .order_by('denominazione')
        return render(request, 'nominati/partecipata_list.html',
                      {'partecipate': partecipate})
